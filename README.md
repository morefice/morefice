### Hello bonjour I'm Max <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px">

<h3>I'm a Senior Backend Engineer from the <span>[Database Framework](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/data-access/database-framework/) team at GitLab.</span></h3>

- I live in Switzerland and my timezone is [CET](https://time.is/CET).