require 'pg'
require 'concurrent-ruby'

def find_bad_rows(min, max, table, connhash, print_mux)
  connection = PG.connect(connhash)
  row_ids = []
  begin
    row_ids = connection.exec("SELECT id FROM #{table} WHERE id BETWEEN #{min} AND #{max}").map { |r| r['id'] }
  rescue PG::DataCorrupted
    print_mux.synchronize do
      puts "WARNING: The ID column on #{table} is corrupt between number #{min} and #{max}, this chunk is unable to be handled"
      row_ids = []
    end
  end
  row_ids.each do |id|
    connection.exec("SELECT * FROM #{table} WHERE id = #{id}")
  rescue PG::DataCorrupted
    print_mux.synchronize do
      puts "Row ID #{id} is corrupt"
    end
  end
end

connhash = {
  dbname: 'corrupted',
  user: 'postgres',
  host: 'localhost'
}

table = 'foo'

print_mutex = Mutex.new

conn = PG.connect(connhash)

cursor = 0
chunk_size = 10_000
t1 = Time.now
threads = []
loop do
  # break if cursor >= max_row_count

  puts "On chunk #{cursor}" if cursor % 1_000_000 == 0

  begin
    end_cursor = cursor + chunk_size - 1
    result = conn.exec("SELECT * FROM #{table} WHERE id BETWEEN #{cursor} AND #{end_cursor}")
    break if result.ntuples == 0
  rescue PG::DataCorrupted
    print_mutex.synchronize do
      puts "Corrupt block found between #{cursor} and #{end_cursor}, locating..."
    end
    threads << Thread.new(cursor, end_cursor) do |thread_cursor, thread_end_cursor|
      find_bad_rows(thread_cursor, thread_end_cursor, table, connhash, print_mutex)
    end
  end

  cursor += chunk_size
end
threads.each(&:join)
t2 = Time.now
puts "Processed in #{t2 - t1} seconds"
