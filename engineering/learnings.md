### Learnings

- [Law of Demeter](https://en.wikipedia.org/wiki/Law_of_Demeter), https://gitlab.com/gitlab-org/gitlab/-/merge_requests/104945

```diff
- build.metadata.enable_debug_trace!
+ build.enable_debug_trace!
```
