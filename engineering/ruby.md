### Ruby

| methods    | definition |
| -------- | ------- |
| [Integer#next](https://rubyapi.org/3.1/o/integer#method-i-succ) | Increment value by 1 |
| [Enumerable#each_cons](https://rubyapi.org/3.1/o/enumerable#method-i-each_cons) | Iterate by x |
| [Comparable](https://rubyapi.org/3.3/o/comparable#method-i-clamp) | Range delimiter values |


#### Measuring performance

```ruby
Benchmark.ips do |x|
  array = [1, 2, 3]

  x.report(:size) { array.size }
  x.report(:length) { array.length }

  x.compare!
end
```

#### Mocking

```ruby
it 'calls the service' do
  service = instance_double(MyService)

  expect(MyService).to receive(:new).with(resource).and_return(service)
  expect(service).to receive(:execute)

  resource.run!
end
```