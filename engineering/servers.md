### Connecting to servers

We use [teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Database_Console_via_Teleport.md) to access remotely our servers in secure way.

#### Staging psql

```
tsh login --proxy=staging.teleport.gitlab.net
tsh db login --db-user=console-ro --db-name=gitlabhq_production db-ci-replica-gstg
tsh db connect db-ci-replica-gstg
```
