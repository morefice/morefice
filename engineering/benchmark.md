### Measuring performance

Use [`hyperfire`](https://github.com/sharkdp/hyperfine) to measure how long it takes to run a program.

```shell
# Checking how much time it takes to perform this operation
hyperfine -p 'git reset && git checkout db' 'bundle exec rake "gitlab:db:squash[origin/16-3-stable-ee]"'
```