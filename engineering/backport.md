### Backport

We can backport a MR to an older version of gitlab when we need to patch a bug.

Here is the [dedicated process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/engineers.md) to follow.

```shell
git checkout 16-9-stable-ee
git checkout -b morefice/backport-bug
git cherry-pick <commit_id>
```
