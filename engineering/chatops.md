### Chatops

[Bot](https://docs.gitlab.com/ee/ci/chatops/) to interact with GitLab through Slack.

#### Enabling a feature flag

```
/chatops run feature set <feature_flag_name> true
```

#### Check the deployment status of a given commit

```
/chatops run auto_deploy status <sha>
```

#### Check if a MR is included within a given milestone

```
/chatops run release check https://gitlab.com/gitlab-org/gitlab/-/merge_requests/<merge_request_id> 15.9
```

#### List status of current migrations
```
/chatops run batched_background_migrations list --production --database ci
```