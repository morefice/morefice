### Rails

| methods    | definition |
| -------- | ------- |
| [redirect_back_or_to](https://api.rubyonrails.org/classes/ActionController/Redirecting.html#method-i-redirect_back_or_to)  | Redirect action or back to current resource |
| [class_names](https://api.rubyonrails.org/v7.0.6/classes/ActionView/Helpers/TagHelper.html#method-i-token_list) | Add css classes dynamically |
| [excluding](https://api.rubyonrails.org/classes/ActiveRecord/QueryMethods.html#method-i-excluding) | Excludes the specify record |