### Database

We use [postgres.ai](console.postgres.ai) to test queries performance on a `thin clone` which is a snapshoot of the database.

#### Connect to a thin clone

```shell
ssh -NTML 6409:localhost:6409 gldatabase@gitlab-joe-poc.postgres.ai -i ~/.ssh/id_ed25519
psql "host=localhost port=6409 user=morefice dbname=gitlabhq_dblab"
```

We can also use the [pgai](https://docs.gitlab.com/ee/development/database/database_lab_pgai.html) gem for this purpose.

### Links

- https://sqlfiddle.com