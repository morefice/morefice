### Security

We have a separate repository to work on fixing security bugs.

Here is the [dedicated process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md) to follow.

