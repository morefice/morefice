### GPG

[Encryption tool](https://en.wikipedia.org/wiki/GNU_Privacy_Guard) used to sign my commits.

Some commands to export existing key and import it to another computer.

```
- gpg --list-secret-keys
- gpg --export-secret-keys > secret-keys.gpg
- gpg --import secret-keys.gpg


- echo "test" | gpg --encrypt -r PRIVATE_KEY > out.gpg
- gpg --decrypt -v out.gpg
```
